# README #

A console app used to query the pi sense HAT:
* Sensor data is logged to a mongo db instance
* A restful api and web app to show reports on the data logged

### How do I get set up? ###

* Clone the repo
* Run "npm install" to install dependencies
* To run the api/web app : run "npm start" OR ./docker-run.sh to run in a container
