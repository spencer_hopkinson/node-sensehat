var nodeimu = require("nodeimu");
var sensor = new nodeimu.IMU();
var interval = process.argv[2] || 5000;
var sensorLocation = process.argv[3] || "not-set";
var ledMatrix = require("raspberry-sensor-sense-hat");
var db = require("./data");

console.log("Interval %d",interval);

setInterval(querySensor,interval);

function querySensor(){
	//console.log('taking reading');
	sensor.getValue(imuCallback);
}

function imuCallback(e,data){
	
	if(e){
		console.log(e);
		return;
	}
	
	var sensorData = {
		dateTime:new Date(),
		temp:data.temperature.toFixed(2),
		humidity:data.humidity.toFixed(2),
		pressure:data.pressure.toFixed(4),
		location:sensorLocation
	};
	
	try{
		ledMatrix.show_message(sensorData.temp,function(err,results){
		});
	}	
	catch(e){
		console.log(e);
	}
	
	console.log(sensorData);

	try{
		db.addSensorData(sensorData);
	}
	catch(e){
		console.log(e);
	}
}
