FROM node:argon
RUN mkdir -p /usr/src/node-api
WORKDIR /usr/src/node-api
RUN mkdir logs
COPY /package.json /usr/src/node-api
RUN npm install --production
COPY /api /usr/src/node-api
CMD ["node","index.js"]
