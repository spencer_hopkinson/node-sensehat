/*
DEFINE API ROUTES AND HANDLERS
*/

var pingHandler = require('./handlers/ping');
var sensorHandler = require('./handlers/sensor');

module.exports={
  setup:function(app){
    app.get('/api/ping',pingHandler.getPing);
    app.get('/api/sensor/lastEntry',sensorHandler.getLastEntry);
    //app.post('/api/ping',null);
  }
}
