var bodyParser = require('body-parser');
var express = require('express');
var app = express();
var routes = require('./routes');
var errorHandler = require('errorhandler');
var config = require('./config');
var fs = require('fs');
var morgan = require('morgan');

//define logging
//noted this is synchronous but this is in a bootup phase so only run once
if(!fs.existsSync('logs')){
  fs.mkdirSync('logs');
}
var expressLogFile = fs.createWriteStream(config.logging.log,{flags:'a'});
app.use(morgan('combined',{stream:expressLogFile}));

app.use(bodyParser.json());

if(app.settings.env==='development'){
  app.use(errorHandler({log:true}));
}

if(app.settings.env==='production'){
  app.use(errorhandler({log:false}));
}

module.exports =
{
  start:function(){
    console.log("setting up routes..");
    routes.setup(app);
    console.log("routes defined.");

    console.log("Std log is: %s",config.logging.log);
    console.log("Error log is: %s",config.logging.errorLog);

    var port = process.env.PORT || 8080;
    app.listen(port);
    //http://expressjs.com/en/api.html#app.settings.table
    console.log("Server listening on port %d in %s mode",port,app.settings.env);
  }
};
