var client = require("mongodb").MongoClient;
var url = "mongodb://homeserver:27017/home";

module.exports = {
	add:function(model,callback){
		client.connect(url,function(err,db){
			if(err){
				callback(err);
			}
			db.collection('sensor').insertOne(model);			
		});
	},
	lastEntry:function(callback){		
		client.connect(url,function(err,db){
			if(err){
				callback(err);
			}
			db.collection('sensor').find().sort({_id:-1}).limit(1).toArray(function(err,docs){
				callback(err,docs);
			});						
		});		
	}		
};

