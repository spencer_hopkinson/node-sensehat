var sinon = require('sinon');
var assert = require('assert');
var routes = require('../../api/routes');

describe("Routes", function() {
  describe("Setup", function() {

      it("should respond with ...", function() {
        var app,spy;

        app = {};
        spy = app.get = app.post = sinon.spy();

        routes.setup(app);
        //console.log(spy);
        assert.equal(true,spy.calledOnce);       //1 route configured         
      });

  });
});
