var sinon = require('sinon');
var assert = require('assert');
var pingHandler = require('../../api/handlers/ping');

describe("Routes", function() {
  describe("Ping", function() {

      it("should respond with PONG", function() {
        var req,res,spy;

        req = res = {};
        spy = res.json = sinon.spy();

        pingHandler.getPing(req, res);
        assert.equal(true,spy.calledOnce);
        assert.equal('PONG',spy.args[0]);
        //console.log(spy);
      });

  });
});
