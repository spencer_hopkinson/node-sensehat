var assert = require('assert');
var db = require('../../data');

describe('Data',function(){  

  before(function(done){
    //console.log('...before...');
    done();
  });

  describe('Add sensor entry',function(){

    /*
    it('should return no error',function(done){
      //console.log(db);
      db.add({location:"lounge"},function(err){
        assert.equal(null,err);
        done();
      })  
    });
    */

    it('should return no error',function(done){
      //console.log(db);
      db.add({location:"int test",dateTime:new Date()},function(err){
        assert.equal(null,err);        
      })  
      done();
    });

    it('should return the last entry',function(done){
      db.lastEntry(function(err,docs){
        assert.notEqual(null,docs);
        //console.log(docs);
        //console.log(docs[0].location);
        assert.equal('int test',docs[0].location);
      });      
      done();
    })
    
    /*
    it('should return PONG in the body',function(done){
		request(url)
		.get('/api/ping')        
		.expect('"PONG"',done)
    })
    */
  });
})