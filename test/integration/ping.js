var assert = require('assert');
var request = require('supertest');

describe('Ping',function(){

	var url = 'http://localhost:8080';

  before(function(done){
    //console.log('...before...');
    done();
  });

  describe('Ping Route',function(){

    it('should return 200',function(done){
      request(url)
      .get('/api/ping')
        // end handles the response
    	.end(function(err, res) {
        if (err) {
          throw err;
        }
        assert.equal(res.status,200);
        done();
      })
    });

    it('should return PONG in the body',function(done){
		request(url)
		.get('/api/ping')        
		.expect('"PONG"',done)
    })
  });
})